### Wittgenstein's Tractatus Concepts Network

On display [here](http://wittgenstein-network.gitlab.io/).

#### Compiling locally

First you need to run `npm install`

#### About

In this repository you'll find the source code for the web app that renders the network of concepts extracted from Wittgenstein's Tractatus.


A report is available [here](https://www.clarin-d.de/images/lt4dh/pdf/LT4DH10.pdf
). If you use this visualization tool for academic purposes, please cite:

```
	@InProceedings{bucur-nisioi:2016:LT4DH,
	  author    = {Bucur, Anca  and  Nisioi, Sergiu},
	  title     = {A Visual Representation of Wittgenstein's Tractatus Logico-Philosophicus},
	  booktitle = {Proceedings of the Workshop on Language Technology Resources and Tools for Digital Humanities (LT4DH)},
	  month     = {December},
	  year      = {2016},
	  address   = {Osaka, Japan},
	  publisher = {The COLING 2016 Organizing Committee},
	  pages     = {71--75},
	  url       = {http://aclweb.org/anthology/W16-4010}
	}
```

#### Related project
[Wittgenstein Tractatus Network](http://tractatus.gitlab.io/)
